import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Pango
import os, sys
import re
secu=3 # par defaut ds le combobox
se=240 # defaut ds la combobox

def is_numeric(num):
    pattern = re.compile(r'^[-+]?[-0-9]\d*\.\d*|[-+]?\.?[0-9]\d*$')
    result = pattern.match(num)
    if result:
        return True
    else:
        return False

def MessageErreur():
            dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO,Gtk.ButtonsType.OK, "Entrer un nombre réél")
            dialog.format_secondary_text("Impossible de faire des calculs sur des valeurs non numériques.")
            dialog.run()
            print("INFO dialog closed")
            dialog.destroy()

def MessageErreurZero():
            dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO,Gtk.ButtonsType.OK, "Entrer un nombre non nul")
            dialog.format_secondary_text("Impossible de faire des divisions avec des valeurs nulles.")
            dialog.run()
            print("INFO dialog closed")
            dialog.destroy()

class Calcul_De_Vis:
    ___gtype_name___ = "Calcul_De_Vis"
    N = ""

    def __init__(self):
        # en cas d'usage de PyInstaller pour créer un exe unique, il faut adapter le chemin du fichier glade
        if getattr(sys, 'frozen', False):
           print("bundle")
           wd = sys._MEIPASS
        else:
           print("live")
           wd = os.getcwd()
        file_path = os.path.join(wd,'Calcul_De_Vis.glade')
        interface = Gtk.Builder()
        interface.add_from_file(file_path)
        interface.connect_signals(self)
        window = interface.get_object("window")
        self.entry_effort = interface.get_object("entry_effort")
        self.entry_nb = interface.get_object("entry_nb")
        self.entry_ktsup = interface.get_object("entry_ktsup")
        self.combo_se = interface.get_object("combo_se")

        self.Seq = interface.get_object("Seq")
        self.Smax = interface.get_object("label_Smax")
        self.Smin = interface.get_object("label_Smin")
        self.Dmax = interface.get_object("label_Dmax")
        self.Dmin = interface.get_object("label_Dmin")
        self.Cons1 = interface.get_object("LabelCons1")
        self.Cons2 = interface.get_object("LabelCons2")
        self.Cons3 = interface.get_object("LabelCons3")
        self.Cons4 = interface.get_object("LabelCons4")
        self.Cons5 = interface.get_object("LabelCons5")
        window.show_all()

    # Lien avec glade : windows / signaux / GtkObject / widget / destroy -> destroy
    def destroy(self, widget):
        print("Au Revoir !")
        Gtk.main_quit()

    def on_effort_activate(self,widget):
        N =  self.entry_effort.get_text()
        print ("champ modifié, N= ",N)
        if not is_numeric(N):
            MessageErreur()

    def on_nb_activate(self,widget):
        nb =  self.entry_nb.get_text()
        print ("champ modifié, nb= ",nb)
        if not is_numeric(nb):
            MessageErreur()

    def on_ktsup_activate(self,widget):
        ktsup =  self.entry_ktsup.get_text()
        print ("champ modifié, nb= ",ktsup)
        if not is_numeric(ktsup):
            MessageErreur()

    def combo_secu_changed(self,widget,data=None):
        global secu # se est une variable Globale
        model=widget.get_model()
        active=widget.get_active()
        if active >=0:
            secu= int(model[active][0])
            print("code : ",secu)
        else:
            print("erreur combo")

    def combo_se_changed(self,widget,data=None):
        global se # se est une variable Globale
        model=widget.get_model()
        active=widget.get_active()
        if active >=0:
            mod = model[active][0]
            se= mod[28:31]
            print("code : ",mod[28:31])
        else:
            print("erreur combo")

    def on_Bouton_Calc_clicked(self,widget):
        erreur=0
        print("bouton pressé")
        N =  self.entry_effort.get_text()
        if not is_numeric(N):
            MessageErreur(); erreur=erreur+1
        nb= self.entry_nb.get_text()
        if not is_numeric(nb):
            MessageErreur(); erreur=erreur+1
        else:
            if nb=="0":
                MessageErreurZero(); erreur=erreur+1
        ktsup= self.entry_ktsup.get_text()
        if not is_numeric(ktsup):
            MessageErreur(); erreur=erreur+1
        # print("Erreur",erreur,"N=",N," nb=",nb,"secu=",secu," se=",se," ktsup=",ktsup)
        if erreur<=0:
            Section_eq=3.3*float(ktsup)*float(N)*float(secu)/(float(se)*float(nb))
            self.Seq.set_label(str(Section_eq))
            # Base de donnée des sections equivalentes
            BSeq_old=(0.46,0.732,1.27,2.07,3.39,5.03,8.78,14.2,20.1,36.6,84.3,157,245,533,561,817,1121,1473,2362,2676)
            BSeq=(0.460,0.588,0.732,0.983,1.27,1.70,2.07,2.48,3.39,5.03,6.78,8.78,11.3,14.2,21.1,28.9,36.6,58.0,84.3,115,157,192,245,303,353,459,561,694,817,976,1121,1306,1473,1758,2030,2362,2676)
            # Base de donnée des diamètres métriques
            DMetrique_old=(1,1.2,1.6,2,2.5,3,4,5,6,10,12,16,20,24,30,36,42,48,56,64)
            DMetrique=(1,(1.1),1.2,(1.4),1.6,(1.8),2,(2.2),2.5,3,(3.5),4,(4.5),5,6,(7),8,10,12,(14),16,(18),20,(22),24,(27),30,(33),36,(39),42,(45),48,(52),56,(60),64)
            DMetrique_second=((1.1),(1.4),(1.8),(2.2),(3.5),(4.5),(7),(14),(18),(22),(27),(33),(39),(45),(52),(60))
            # la section est elle avant le premier élément de la liste ?
            if BSeq[0]>=Section_eq:
                BSeq_min=0
                BSeq_max=BSeq[0]
                DMetrique_min=0
                DMetrique_max=DMetrique[0]
            # La section est elle aprés le dernier élément de la liste ?
            elif BSeq[len(BSeq)-1]<=Section_eq:
                BSeq_min=BSeq[len(BSeq)-1]
                BSeq_max=0
                DMetrique_min=DMetrique[len(BSeq)-1]
                DMetrique_max=0
            # la section est forcement dans la liste !
            else:
                i=0
                while i<len(BSeq):
                    if Section_eq>=BSeq[i] and Section_eq<=BSeq[i+1]:
                        print ("trouvé à ",i," entre ",BSeq[i]," et ",BSeq[i+1])
                        BSeq_min=BSeq[i]
                        DMetrique_min=DMetrique[i]
                        BSeq_max=BSeq[i+1]
                        DMetrique_max=DMetrique[i+1]
                        #prevoir le cas au on arrive a la limite haute des diamètres normalisés (conseil de choix métrique)
                        if i<len(BSeq)-2:
                            DMetrique_sup=DMetrique[i+2]
                        print(DMetrique_min,"-",DMetrique_max)
                    i=i+1
            print("Smin=",BSeq_min,"- Smax=",BSeq_max," - DMetrique_min",DMetrique_min," - DMetrique_max",DMetrique_max)
            # mise a jour de l'interface
            # Sections
            string='<span color="red" weight="bold" size="xx-large">{0}</span>'.format(BSeq_min)
            self.Smin.set_markup(str(string))
            string='<span color="blue" weight="bold" size="xx-large">{0}</span>'.format(BSeq_max)
            self.Smax.set_markup(str(string))
            # Diamètres
            string='<span color="red" weight="bold" size="xx-large">{0}</span>'.format(DMetrique_min)
            self.Dmin.set_markup(str(string))
            string='<span color="blue" weight="bold" size="xx-large">{0}</span>'.format(DMetrique_max)
            self.Dmax.set_markup(str(string))
            # conseils
            self.Cons1.set_label("")
            self.Cons2.set_label("")
            self.Cons3.set_label("")
            self.Cons4.set_label("")
            self.Cons5.set_label("")

            if Section_eq-BSeq_min<BSeq_min/10:
                print("petit")
                self.Cons1.set_label("Petit écart avec Seq inférieure, augmenter")
                self.Cons2.set_label("le nombre de vis ou prendre des vis de meilleure qualité.")
            if BSeq_max-Section_eq<BSeq_max/10 and BSeq_max !=0:
                print("grand : DSeq_max=",BSeq_max," Section_eq=",Section_eq)
                self.Cons1.set_label("Petit écart avec Seq sup, solution optimale.")
            if BSeq_max==0:
                self.Cons1.set_label("Même la vis la plus grande ne peut convenir.")
                self.Cons2.set_label(" - Augmenter le nombre de vis.")
                self.Cons3.set_label(" - Augmenter la qualité des vis.")
            j=0
            while j<len(DMetrique_second):
                if DMetrique_min==DMetrique_second[j]:
                    self.Cons4.set_label("(Le diàmétre inférieur est une valeur non conseillée)")
                if DMetrique_max==DMetrique_second[j]:
                    self.Cons4.set_label("Le diàmétre supérieur est une valeur non conseillée !")
                    self.Cons5.set_label("Le diàmétre normalisé supérieur est M{0} mm".format(DMetrique_sup))
                j=j+1
if __name__ == "__main__":
    app = Calcul_De_Vis()
    Gtk.main()